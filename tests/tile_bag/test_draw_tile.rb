require 'minitest/autorun'
require_relative "../../tile_bag.rb"

#Tests the 'draw_tile' method of class TileBag.
class TileBag::TestDrawTile < MiniTest::Test

	#Tests that 98 tiles can be drawn and
	#then the bag is empty
	def test_has_proper_number_of_tiles
		bag = TileBag.new
		98.times do |i|
			assert_equal !bag.empty?
			bag.draw_tile
		end
		assert_equal bag.empty?
	end

	#Tests that the tiles are properly
	#distributed
	def test_has_proper_tile_distribution
		
	end
end
