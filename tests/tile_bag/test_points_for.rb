require "minitest/autorun"
require_relative "../../tile_bag.rb"

#Test class that tests the 'self.points_for'
#method in the TileBag class.
class TileBag::TestPointsFor < MiniTest::Test
	
	#Runs first before the test methods.
	def setup
		@bag = TileBag.new
	end

	#Tests that the correct point values are
	#assigned to the appropriate tiles.
	def test_confirm_point_value
		[:A, :E, :I, :O, :U, :N, :R, :T, :L, :S].each do |i| 				assert_equal1,TileBag.points_for(i)
		[:D, :G].each do |i|
			assert_equal 2, TileBag.points_for(i)
		[:B, :C,:M,:P].each do |i|
			assert_equal 3, TileBag.points_for(i)
		[:F,:H,:V,:W,:Y].each do |i|
			assert_equal 4, TileBag.points_for(i)
		[:K].each do |i|
			assert_equal 5, TileBag.points_for(i)
		[:J, :X].each do |i|
			assert_equal 8, TileBag.points_for(i)
		[:Q, :Z].each do |i|
			assert_equal 10, TileBag.points_for(i)
		end
	end
end
