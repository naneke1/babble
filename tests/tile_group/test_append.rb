require 'minitest/autorun'
require_relative '../../tile_group.rb'

#
class TestAppend < Minitest::Test
	
	#
	def setup
		@group = TileGroup.new
	end

	#
	def test_append_one_tile
		@group.append :A
		assert_equal [:A], @group.tiles
	end

	#
	def test_append_many_tiles
		@group.append :A
		@group.append :B
		@group.append :C
		@group.append :D
		@group.append :E
		assert_equal [:A, :B, :C, :D, :E], @group.tiles
	end

	#
	def test_append_duplicate_tiles
		@group.append :A
		@group.append :B
		@group.append :A
		@group.append :D
		@group.append :A
		assert_equal [:A, :B, :A, :D, :A], @group.tiles
	end
end
