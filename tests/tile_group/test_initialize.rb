require 'minitest/autorun'
require_relative '../../tile_group.rb'

#
class TestInitialize < Minitest::Test

	#
	def test_create_empty_tile_group
		group = TileGroup.new
		assert 0, group.tiles.size
	end

end
