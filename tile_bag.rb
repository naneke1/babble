#A class that handles a bag containing 98 tiles
#of letters and associates the tiles with their
#corresponding points.
class TileBag

	#Constructs a new TileBag object.
	def initialize
		@tiles =[:A,:A,:A,:A,:A,:A,:A,:A,:A,:B,:B,:C,:C,
			:D,:D,:D,:D,:E,:E,:E,:E,:E,:E,:E,:E,:E,:E,:E,:E,
			:F,:F,:G,:G,:G,:H,:H,:I,:I,:I,:I,:I,:I,:I,:I,:I,
			:J,:K,:L,:L,:L,:L,:M,:M,:N,:N,:N,:N,:N,:N,
			:O,:O,:O,:O,:O,:O,:O,:O,:P,:P,:Q,:R,:R,:R,:R,:R,:R,
			:S,:S,:S,:S,:T,:T,:T,:T,:T,:T,:U,:U,:U,:U,:V,:V,
			:W,:W,:X,:Y,:Y,:Z]	
	end

	#Removes a random tile from the tile bag and returns it.
	def draw_tile
		@tiles.pop
	end
	
	#Checks if the tile bag is empty and returns
	#true or false.
	def empty?
		return @tiles.empty?
	end

	#Assigns each letter in the tile bag with a point value.
	def self.points_for(tile)
		hash = {:A => 1, :B => 3, :C => 3, :D => 2, :E => 1,
			:F => 4, :G => 2, :H => 4, :I => 1, :J => 8,
			:K => 5, :L => 1, :M => 3, :N => 1, :O => 1,
			:P => 3, :Q => 10, :R => 1, :S => 1,:T => 1,
			:U => 1, :V => 4, :W => 4, :X => 8, :Y => 4, :Z => 10}
		return hash[tile]
	end
end
