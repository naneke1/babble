require_relative 'tile_group'
require_relative 'tile_bag'

#
class Word < TileGroup

	#
	def initialize
		super
	end

	#
	def score
		points = 0
		@tiles.each { |i| points += TileBag::points_for(i)}
		points
	end
end
